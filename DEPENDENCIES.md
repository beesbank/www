
* 84 Maven dependencies
* 277 NPM dependencies


```
mvn dependency:list
[jenkins-maven-event-spy] INFO generate /Users/cyrilleleclerc/git/jenkins-world/www/maven-spy-20170908-142659-7698019012069785733230.log.tmp ...
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building www 1.0.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO]
[INFO] --- maven-dependency-plugin:2.10:list (default-cli) @ www ---
[INFO]
[INFO] The following files have been resolved:
[INFO]    com.fasterxml.jackson.core:jackson-annotations:jar:2.8.0:compile
[INFO]    org.codehaus.groovy:groovy:jar:2.4.12:compile
[INFO]    org.springframework.boot:spring-boot-test:jar:1.5.6.RELEASE:test
[INFO]    org.jboss.logging:jboss-logging:jar:3.3.1.Final:compile
[INFO]    com.beesbank.service:account-access-service:jar:1.0.1-SNAPSHOT:test
[INFO]    org.hamcrest:hamcrest-library:jar:1.3:test
[INFO]    org.hibernate.common:hibernate-commons-annotations:jar:5.0.1.Final:compile
[INFO]    org.apache.tomcat:tomcat-juli:jar:8.5.16:compile
[INFO]    org.mockito:mockito-core:jar:1.10.19:test
[INFO]    org.springframework.hateoas:spring-hateoas:jar:0.23.0.RELEASE:compile
[INFO]    org.springframework.data:spring-data-jpa:jar:1.11.6.RELEASE:compile
[INFO]    com.beesbank.service:atm-by-zip-service:jar:1.0.2-SNAPSHOT:test
[INFO]    org.springframework.boot:spring-boot-starter-thymeleaf:jar:1.5.6.RELEASE:compile
[INFO]    antlr:antlr:jar:2.7.7:compile
[INFO]    org.assertj:assertj-core:jar:2.6.0:test
[INFO]    nz.net.ultraq.thymeleaf:thymeleaf-layout-dialect:jar:1.4.0:compile
[INFO]    org.skyscreamer:jsonassert:jar:1.4.0:test
[INFO]    org.yaml:snakeyaml:jar:1.17:runtime
[INFO]    org.springframework.boot:spring-boot:jar:1.5.6.RELEASE:compile
[INFO]    org.javassist:javassist:jar:3.21.0-GA:compile
[INFO]    org.slf4j:jcl-over-slf4j:jar:1.7.25:compile
[INFO]    org.apache.tomcat.embed:tomcat-embed-websocket:jar:8.5.16:compile
[INFO]    org.springframework.boot:spring-boot-starter-test:jar:1.5.6.RELEASE:test
[INFO]    org.springframework.boot:spring-boot-starter-tomcat:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework:spring-expression:jar:4.3.10.RELEASE:compile
[INFO]    org.aspectj:aspectjweaver:jar:1.8.10:compile
[INFO]    org.hamcrest:hamcrest-core:jar:1.3:test
[INFO]    org.hibernate:hibernate-entitymanager:jar:5.0.12.Final:compile
[INFO]    com.jayway.jsonpath:json-path:jar:2.2.0:test
[INFO]    org.springframework:spring-jdbc:jar:4.3.10.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-starter-data-rest:jar:1.5.6.RELEASE:compile
[INFO]    com.h2database:h2:jar:1.4.196:runtime
[INFO]    org.apache.tomcat.embed:tomcat-embed-core:jar:8.5.16:compile
[INFO]    org.springframework.boot:spring-boot-devtools:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework.data:spring-data-commons:jar:1.13.6.RELEASE:compile
[INFO]    org.springframework:spring-context:jar:4.3.10.RELEASE:compile
[INFO]    org.apache.tomcat.embed:tomcat-embed-el:jar:8.5.16:compile
[INFO]    com.beesbank.service:nearest-atm-service:jar:1.0.1-SNAPSHOT:test
[INFO]    org.hibernate:hibernate-core:jar:5.0.12.Final:compile
[INFO]    org.hibernate:hibernate-validator:jar:5.3.5.Final:compile
[INFO]    javax.validation:validation-api:jar:1.1.0.Final:compile
[INFO]    dom4j:dom4j:jar:1.6.1:compile
[INFO]    org.springframework.data:spring-data-rest-core:jar:2.6.6.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-starter-logging:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-starter-aop:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-starter-jdbc:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework.data:spring-data-rest-webmvc:jar:2.6.6.RELEASE:compile
[INFO]    org.unbescape:unbescape:jar:1.1.0.RELEASE:compile
[INFO]    org.ow2.asm:asm:jar:5.0.3:test
[INFO]    org.slf4j:log4j-over-slf4j:jar:1.7.25:compile
[INFO]    org.springframework.boot:spring-boot-starter-data-jpa:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework:spring-orm:jar:4.3.10.RELEASE:compile
[INFO]    ch.qos.logback:logback-core:jar:1.1.11:compile
[INFO]    org.springframework:spring-aspects:jar:4.3.10.RELEASE:compile
[INFO]    org.springframework:spring-core:jar:4.3.10.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-test-autoconfigure:jar:1.5.6.RELEASE:test
[INFO]    org.hibernate.javax.persistence:hibernate-jpa-2.1-api:jar:1.0.0.Final:compile
[INFO]    com.fasterxml.jackson.core:jackson-databind:jar:2.8.9:compile
[INFO]    org.thymeleaf:thymeleaf-spring4:jar:2.1.5.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-starter:jar:1.5.6.RELEASE:compile
[INFO]    org.thymeleaf:thymeleaf:jar:2.1.5.RELEASE:compile
[INFO]    org.springframework:spring-aop:jar:4.3.10.RELEASE:compile
[INFO]    org.objenesis:objenesis:jar:2.1:test
[INFO]    org.apache.tomcat:tomcat-jdbc:jar:8.5.16:compile
[INFO]    org.springframework:spring-beans:jar:4.3.10.RELEASE:compile
[INFO]    org.jboss:jandex:jar:2.0.0.Final:compile
[INFO]    org.springframework.boot:spring-boot-autoconfigure:jar:1.5.6.RELEASE:compile
[INFO]    ognl:ognl:jar:3.0.8:compile
[INFO]    org.springframework:spring-webmvc:jar:4.3.10.RELEASE:compile
[INFO]    org.atteo:evo-inflector:jar:1.2.2:compile
[INFO]    junit:junit:jar:4.12:test
[INFO]    org.springframework:spring-tx:jar:4.3.10.RELEASE:compile
[INFO]    org.slf4j:slf4j-api:jar:1.7.25:compile
[INFO]    javax.transaction:javax.transaction-api:jar:1.2:compile
[INFO]    net.minidev:json-smart:jar:2.2.1:test
[INFO]    net.minidev:accessors-smart:jar:1.1:test
[INFO]    org.springframework.plugin:spring-plugin-core:jar:1.2.0.RELEASE:compile
[INFO]    org.springframework.boot:spring-boot-starter-web:jar:1.5.6.RELEASE:compile
[INFO]    org.springframework:spring-test:jar:4.3.10.RELEASE:test
[INFO]    com.vaadin.external.google:android-json:jar:0.0.20131108.vaadin1:test
[INFO]    com.fasterxml:classmate:jar:1.3.3:compile
[INFO]    ch.qos.logback:logback-classic:jar:1.1.11:compile
[INFO]    org.springframework:spring-web:jar:4.3.10.RELEASE:compile
[INFO]    com.fasterxml.jackson.core:jackson-core:jar:2.8.9:compile
[INFO]    org.slf4j:jul-to-slf4j:jar:1.7.25:compile
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.838 s
[INFO] Finished at: 2017-09-08T14:27:03+02:00
[INFO] Final Memory: 25M/319M
[INFO] ------------------------------------------------------------------------
```

277 NPM production dependencies

```
npm list --production
beesbank-www@1.0.0 /Users/cyrilleleclerc/git/jenkins-world/www
├─┬ react@15.6.1
│ ├─┬ create-react-class@15.6.0
│ │ ├── fbjs@0.8.14 deduped
│ │ ├── loose-envify@1.3.1 deduped
│ │ └── object-assign@4.1.1 deduped
│ ├─┬ fbjs@0.8.14
│ │ ├── core-js@1.2.7
│ │ ├─┬ isomorphic-fetch@2.2.1
│ │ │ ├─┬ node-fetch@1.7.2
│ │ │ │ ├─┬ encoding@0.1.12
│ │ │ │ │ └── iconv-lite@0.4.18
│ │ │ │ └── is-stream@1.1.0
│ │ │ └── whatwg-fetch@2.0.3
│ │ ├── loose-envify@1.3.1 deduped
│ │ ├── object-assign@4.1.1 deduped
│ │ ├─┬ promise@7.3.1
│ │ │ └── asap@2.0.6
│ │ ├── setimmediate@1.0.5
│ │ └── ua-parser-js@0.7.14
│ ├─┬ loose-envify@1.3.1
│ │ └── js-tokens@3.0.2
│ ├── object-assign@4.1.1
│ └─┬ prop-types@15.5.10
│   ├── fbjs@0.8.14 deduped
│   └── loose-envify@1.3.1 deduped
├─┬ react-dom@15.6.1
│ ├── fbjs@0.8.14 deduped
│ ├── loose-envify@1.3.1 deduped
│ ├── object-assign@4.1.1 deduped
│ └── prop-types@15.5.10 deduped
├─┬ rest@1.3.2
│ └── when@3.7.8
└─┬ webpack@1.15.0
  ├── acorn@3.3.0
  ├── async@1.5.2
  ├── clone@1.0.2
  ├─┬ enhanced-resolve@0.9.1
  │ ├── graceful-fs@4.1.11
  │ ├── memory-fs@0.2.0
  │ └── tapable@0.1.10 deduped
  ├── interpret@0.6.6
  ├─┬ loader-utils@0.2.17
  │ ├── big.js@3.1.3
  │ ├── emojis-list@2.1.0
  │ ├── json5@0.5.1
  │ └── object-assign@4.1.1 deduped
  ├─┬ memory-fs@0.3.0
  │ ├─┬ errno@0.1.4
  │ │ └── prr@0.0.0
  │ └─┬ readable-stream@2.3.3
  │   ├── core-util-is@1.0.2
  │   ├── inherits@2.0.3
  │   ├── isarray@1.0.0
  │   ├── process-nextick-args@1.0.7
  │   ├── safe-buffer@5.1.1
  │   ├─┬ string_decoder@1.0.3
  │   │ └── safe-buffer@5.1.1 deduped
  │   └── util-deprecate@1.0.2
  ├─┬ mkdirp@0.5.1
  │ └── minimist@0.0.8
  ├─┬ node-libs-browser@0.7.0
  │ ├─┬ assert@1.4.1
  │ │ └── util@0.10.3 deduped
  │ ├─┬ browserify-zlib@0.1.4
  │ │ └── pako@0.2.9
  │ ├─┬ buffer@4.9.1
  │ │ ├── base64-js@1.2.1
  │ │ ├── ieee754@1.1.8
  │ │ └── isarray@1.0.0 deduped
  │ ├─┬ console-browserify@1.1.0
  │ │ └── date-now@0.1.4
  │ ├── constants-browserify@1.0.0
  │ ├─┬ crypto-browserify@3.3.0
  │ │ ├─┬ browserify-aes@0.4.0
  │ │ │ └── inherits@2.0.3 deduped
  │ │ ├── pbkdf2-compat@2.0.1
  │ │ ├── ripemd160@0.2.0
  │ │ └── sha.js@2.2.6
  │ ├── domain-browser@1.1.7
  │ ├── events@1.1.1
  │ ├── https-browserify@0.0.1
  │ ├── os-browserify@0.2.1
  │ ├── path-browserify@0.0.0
  │ ├── process@0.11.10
  │ ├── punycode@1.4.1
  │ ├── querystring-es3@0.2.1
  │ ├── readable-stream@2.3.3 deduped
  │ ├─┬ stream-browserify@2.0.1
  │ │ ├── inherits@2.0.3 deduped
  │ │ └── readable-stream@2.3.3 deduped
  │ ├─┬ stream-http@2.7.2
  │ │ ├── builtin-status-codes@3.0.0
  │ │ ├── inherits@2.0.3 deduped
  │ │ ├── readable-stream@2.3.3 deduped
  │ │ ├── to-arraybuffer@1.0.1
  │ │ └── xtend@4.0.1
  │ ├── string_decoder@0.10.31
  │ ├─┬ timers-browserify@2.0.3
  │ │ ├─┬ global@4.3.2
  │ │ │ ├─┬ min-document@2.19.0
  │ │ │ │ └── dom-walk@0.1.1
  │ │ │ └── process@0.5.2
  │ │ └── setimmediate@1.0.5 deduped
  │ ├── tty-browserify@0.0.0
  │ ├─┬ url@0.11.0
  │ │ ├── punycode@1.3.2
  │ │ └── querystring@0.2.0
  │ ├─┬ util@0.10.3
  │ │ └── inherits@2.0.1
  │ └─┬ vm-browserify@0.0.4
  │   └── indexof@0.0.1
  ├─┬ optimist@0.6.1
  │ ├── minimist@0.0.8 deduped
  │ └── wordwrap@0.0.3
  ├─┬ supports-color@3.2.3
  │ └── has-flag@1.0.0
  ├── tapable@0.1.10
  ├─┬ uglify-js@2.7.5
  │ ├── async@0.2.10
  │ ├── source-map@0.5.6
  │ ├── uglify-to-browserify@1.0.2
  │ └─┬ yargs@3.10.0
  │   ├── camelcase@1.2.1
  │   ├─┬ cliui@2.1.0
  │   │ ├─┬ center-align@0.1.3
  │   │ │ ├─┬ align-text@0.1.4
  │   │ │ │ ├── kind-of@3.2.2 deduped
  │   │ │ │ ├── longest@1.0.1
  │   │ │ │ └── repeat-string@1.6.1
  │   │ │ └── lazy-cache@1.0.4
  │   │ ├─┬ right-align@0.1.3
  │   │ │ └── align-text@0.1.4 deduped
  │   │ └── wordwrap@0.0.2
  │   ├── decamelize@1.2.0
  │   └── window-size@0.1.0
  ├─┬ watchpack@0.2.9
  │ ├── async@0.9.2
  │ ├─┬ chokidar@1.7.0
  │ │ ├─┬ anymatch@1.3.2
  │ │ │ ├─┬ micromatch@2.3.11
  │ │ │ │ ├─┬ arr-diff@2.0.0
  │ │ │ │ │ └── arr-flatten@1.1.0
  │ │ │ │ ├── array-unique@0.2.1
  │ │ │ │ ├─┬ braces@1.8.5
  │ │ │ │ │ ├─┬ expand-range@1.8.2
  │ │ │ │ │ │ └─┬ fill-range@2.2.3
  │ │ │ │ │ │   ├─┬ is-number@2.1.0
  │ │ │ │ │ │   │ └── kind-of@3.2.2 deduped
  │ │ │ │ │ │   ├─┬ isobject@2.1.0
  │ │ │ │ │ │   │ └── isarray@1.0.0 deduped
  │ │ │ │ │ │   ├─┬ randomatic@1.1.7
  │ │ │ │ │ │   │ ├─┬ is-number@3.0.0
  │ │ │ │ │ │   │ │ └─┬ kind-of@3.2.2
  │ │ │ │ │ │   │ │   └── is-buffer@1.1.5 deduped
  │ │ │ │ │ │   │ └─┬ kind-of@4.0.0
  │ │ │ │ │ │   │   └── is-buffer@1.1.5 deduped
  │ │ │ │ │ │   ├── repeat-element@1.1.2 deduped
  │ │ │ │ │ │   └── repeat-string@1.6.1 deduped
  │ │ │ │ │ ├── preserve@0.2.0
  │ │ │ │ │ └── repeat-element@1.1.2
  │ │ │ │ ├─┬ expand-brackets@0.1.5
  │ │ │ │ │ └── is-posix-bracket@0.1.1
  │ │ │ │ ├─┬ extglob@0.3.2
  │ │ │ │ │ └── is-extglob@1.0.0 deduped
  │ │ │ │ ├── filename-regex@2.0.1
  │ │ │ │ ├── is-extglob@1.0.0 deduped
  │ │ │ │ ├── is-glob@2.0.1 deduped
  │ │ │ │ ├─┬ kind-of@3.2.2
  │ │ │ │ │ └── is-buffer@1.1.5
  │ │ │ │ ├── normalize-path@2.1.1 deduped
  │ │ │ │ ├─┬ object.omit@2.0.1
  │ │ │ │ │ ├─┬ for-own@0.1.5
  │ │ │ │ │ │ └── for-in@1.0.2
  │ │ │ │ │ └── is-extendable@0.1.1
  │ │ │ │ ├─┬ parse-glob@3.0.4
  │ │ │ │ │ ├─┬ glob-base@0.3.0
  │ │ │ │ │ │ ├── glob-parent@2.0.0 deduped
  │ │ │ │ │ │ └── is-glob@2.0.1 deduped
  │ │ │ │ │ ├── is-dotfile@1.0.3
  │ │ │ │ │ ├── is-extglob@1.0.0 deduped
  │ │ │ │ │ └── is-glob@2.0.1 deduped
  │ │ │ │ └─┬ regex-cache@0.4.3
  │ │ │ │   ├─┬ is-equal-shallow@0.1.3
  │ │ │ │   │ └── is-primitive@2.0.0 deduped
  │ │ │ │   └── is-primitive@2.0.0
  │ │ │ └─┬ normalize-path@2.1.1
  │ │ │   └── remove-trailing-separator@1.0.2
  │ │ ├── async-each@1.0.1
  │ │ ├─┬ fsevents@1.1.2
  │ │ │ ├── nan@2.6.2
  │ │ │ └─┬ node-pre-gyp@0.6.36
  │ │ │   ├─┬ mkdirp@0.5.1
  │ │ │   │ └── minimist@0.0.8
  │ │ │   ├─┬ nopt@4.0.1
  │ │ │   │ ├── abbrev@1.1.0
  │ │ │   │ └─┬ osenv@0.1.4
  │ │ │   │   ├── os-homedir@1.0.2
  │ │ │   │   └── os-tmpdir@1.0.2
  │ │ │   ├─┬ npmlog@4.1.0
  │ │ │   │ ├─┬ are-we-there-yet@1.1.4
  │ │ │   │ │ ├── delegates@1.0.0
  │ │ │   │ │ └── readable-stream@2.2.9 deduped
  │ │ │   │ ├── console-control-strings@1.1.0
  │ │ │   │ ├─┬ gauge@2.7.4
  │ │ │   │ │ ├── aproba@1.1.1
  │ │ │   │ │ ├── console-control-strings@1.1.0 deduped
  │ │ │   │ │ ├── has-unicode@2.0.1
  │ │ │   │ │ ├── object-assign@4.1.1
  │ │ │   │ │ ├── signal-exit@3.0.2
  │ │ │   │ │ ├─┬ string-width@1.0.2
  │ │ │   │ │ │ ├── code-point-at@1.1.0
  │ │ │   │ │ │ ├─┬ is-fullwidth-code-point@1.0.0
  │ │ │   │ │ │ │ └── number-is-nan@1.0.1
  │ │ │   │ │ │ └── strip-ansi@3.0.1 deduped
  │ │ │   │ │ ├─┬ strip-ansi@3.0.1
  │ │ │   │ │ │ └── ansi-regex@2.1.1
  │ │ │   │ │ └─┬ wide-align@1.1.2
  │ │ │   │ │   └── string-width@1.0.2 deduped
  │ │ │   │ └── set-blocking@2.0.0
  │ │ │   ├─┬ rc@1.2.1
  │ │ │   │ ├── deep-extend@0.4.2
  │ │ │   │ ├── ini@1.3.4
  │ │ │   │ ├── minimist@1.2.0
  │ │ │   │ └── strip-json-comments@2.0.1
  │ │ │   ├─┬ request@2.81.0
  │ │ │   │ ├── aws-sign2@0.6.0
  │ │ │   │ ├── aws4@1.6.0
  │ │ │   │ ├── caseless@0.12.0
  │ │ │   │ ├─┬ combined-stream@1.0.5
  │ │ │   │ │ └── delayed-stream@1.0.0
  │ │ │   │ ├── extend@3.0.1
  │ │ │   │ ├── forever-agent@0.6.1
  │ │ │   │ ├─┬ form-data@2.1.4
  │ │ │   │ │ ├── asynckit@0.4.0
  │ │ │   │ │ ├── combined-stream@1.0.5 deduped
  │ │ │   │ │ └── mime-types@2.1.15 deduped
  │ │ │   │ ├─┬ har-validator@4.2.1
  │ │ │   │ │ ├─┬ ajv@4.11.8
  │ │ │   │ │ │ ├── co@4.6.0
  │ │ │   │ │ │ └─┬ json-stable-stringify@1.0.1
  │ │ │   │ │ │   └── jsonify@0.0.0
  │ │ │   │ │ └── har-schema@1.0.5
  │ │ │   │ ├─┬ hawk@3.1.3
  │ │ │   │ │ ├─┬ boom@2.10.1
  │ │ │   │ │ │ └── hoek@2.16.3 deduped
  │ │ │   │ │ ├─┬ cryptiles@2.0.5
  │ │ │   │ │ │ └── boom@2.10.1 deduped
  │ │ │   │ │ ├── hoek@2.16.3
  │ │ │   │ │ └─┬ sntp@1.0.9
  │ │ │   │ │   └── hoek@2.16.3 deduped
  │ │ │   │ ├─┬ http-signature@1.1.1
  │ │ │   │ │ ├── assert-plus@0.2.0
  │ │ │   │ │ ├─┬ jsprim@1.4.0
  │ │ │   │ │ │ ├── assert-plus@1.0.0
  │ │ │   │ │ │ ├── extsprintf@1.0.2
  │ │ │   │ │ │ ├── json-schema@0.2.3
  │ │ │   │ │ │ └─┬ verror@1.3.6
  │ │ │   │ │ │   └── extsprintf@1.0.2 deduped
  │ │ │   │ │ └─┬ sshpk@1.13.0
  │ │ │   │ │   ├── asn1@0.2.3
  │ │ │   │ │   ├── assert-plus@1.0.0
  │ │ │   │ │   ├─┬ bcrypt-pbkdf@1.0.1
  │ │ │   │ │   │ └── tweetnacl@0.14.5 deduped
  │ │ │   │ │   ├─┬ dashdash@1.14.1
  │ │ │   │ │   │ └── assert-plus@1.0.0
  │ │ │   │ │   ├─┬ ecc-jsbn@0.1.1
  │ │ │   │ │   │ └── jsbn@0.1.1 deduped
  │ │ │   │ │   ├─┬ getpass@0.1.7
  │ │ │   │ │   │ └── assert-plus@1.0.0
  │ │ │   │ │   ├─┬ jodid25519@1.0.2
  │ │ │   │ │   │ └── jsbn@0.1.1 deduped
  │ │ │   │ │   ├── jsbn@0.1.1
  │ │ │   │ │   └── tweetnacl@0.14.5
  │ │ │   │ ├── is-typedarray@1.0.0
  │ │ │   │ ├── isstream@0.1.2
  │ │ │   │ ├── json-stringify-safe@5.0.1
  │ │ │   │ ├─┬ mime-types@2.1.15
  │ │ │   │ │ └── mime-db@1.27.0
  │ │ │   │ ├── oauth-sign@0.8.2
  │ │ │   │ ├── performance-now@0.2.0
  │ │ │   │ ├── qs@6.4.0
  │ │ │   │ ├── safe-buffer@5.0.1
  │ │ │   │ ├── stringstream@0.0.5
  │ │ │   │ ├─┬ tough-cookie@2.3.2
  │ │ │   │ │ └── punycode@1.4.1
  │ │ │   │ ├─┬ tunnel-agent@0.6.0
  │ │ │   │ │ └── safe-buffer@5.0.1 deduped
  │ │ │   │ └── uuid@3.0.1
  │ │ │   ├─┬ rimraf@2.6.1
  │ │ │   │ └─┬ glob@7.1.2
  │ │ │   │   ├── fs.realpath@1.0.0
  │ │ │   │   ├─┬ inflight@1.0.6
  │ │ │   │   │ ├── once@1.4.0 deduped
  │ │ │   │   │ └── wrappy@1.0.2 deduped
  │ │ │   │   ├── inherits@2.0.3 deduped
  │ │ │   │   ├─┬ minimatch@3.0.4
  │ │ │   │   │ └─┬ brace-expansion@1.1.7
  │ │ │   │   │   ├── balanced-match@0.4.2
  │ │ │   │   │   └── concat-map@0.0.1
  │ │ │   │   ├── once@1.4.0 deduped
  │ │ │   │   └── path-is-absolute@1.0.1
  │ │ │   ├── semver@5.3.0
  │ │ │   ├─┬ tar@2.2.1
  │ │ │   │ ├─┬ block-stream@0.0.9
  │ │ │   │ │ └── inherits@2.0.3 deduped
  │ │ │   │ ├─┬ fstream@1.0.11
  │ │ │   │ │ ├── graceful-fs@4.1.11
  │ │ │   │ │ ├── inherits@2.0.3 deduped
  │ │ │   │ │ ├── mkdirp@0.5.1 deduped
  │ │ │   │ │ └── rimraf@2.6.1 deduped
  │ │ │   │ └── inherits@2.0.3
  │ │ │   └─┬ tar-pack@3.4.0
  │ │ │     ├─┬ debug@2.6.8
  │ │ │     │ └── ms@2.0.0
  │ │ │     ├── fstream@1.0.11 deduped
  │ │ │     ├─┬ fstream-ignore@1.0.5
  │ │ │     │ ├── fstream@1.0.11 deduped
  │ │ │     │ ├── inherits@2.0.3 deduped
  │ │ │     │ └── minimatch@3.0.4 deduped
  │ │ │     ├─┬ once@1.4.0
  │ │ │     │ └── wrappy@1.0.2
  │ │ │     ├─┬ readable-stream@2.2.9
  │ │ │     │ ├── buffer-shims@1.0.0
  │ │ │     │ ├── core-util-is@1.0.2
  │ │ │     │ ├── inherits@2.0.3 deduped
  │ │ │     │ ├── isarray@1.0.0
  │ │ │     │ ├── process-nextick-args@1.0.7
  │ │ │     │ ├─┬ string_decoder@1.0.1
  │ │ │     │ │ └── safe-buffer@5.0.1 deduped
  │ │ │     │ └── util-deprecate@1.0.2
  │ │ │     ├── rimraf@2.6.1 deduped
  │ │ │     ├── tar@2.2.1 deduped
  │ │ │     └── uid-number@0.0.6
  │ │ ├─┬ glob-parent@2.0.0
  │ │ │ └── is-glob@2.0.1 deduped
  │ │ ├── inherits@2.0.3 deduped
  │ │ ├─┬ is-binary-path@1.0.1
  │ │ │ └── binary-extensions@1.9.0
  │ │ ├─┬ is-glob@2.0.1
  │ │ │ └── is-extglob@1.0.0
  │ │ ├── path-is-absolute@1.0.1
  │ │ └─┬ readdirp@2.1.0
  │ │   ├── graceful-fs@4.1.11 deduped
  │ │   ├─┬ minimatch@3.0.4
  │ │   │ └─┬ brace-expansion@1.1.8
  │ │   │   ├── balanced-match@1.0.0
  │ │   │   └── concat-map@0.0.1
  │ │   ├── readable-stream@2.3.3 deduped
  │ │   └── set-immediate-shim@1.0.1
  │ └── graceful-fs@4.1.11 deduped
  └─┬ webpack-core@0.6.9
    ├── source-list-map@0.1.8
    └─┬ source-map@0.4.4
      └── amdefine@1.0.1
``` 