'use strict';

// tag::vars[]
const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
// end::vars[]

// tag::app[]
class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {atms: [], accounts: []};
	}

	componentDidMount() {

        client({method: 'GET', path: '/api/atms'}).done(response => {
            this.setState({atms: response.entity._embedded.atms});
        });

        client({method: 'GET', path: '/api/accounts'}).done(response => {
            this.setState({accounts: response.entity._embedded.accounts});
        });
	}

	render() {
		return (
			<div className="main">
				<AtmList atms={this.state.atms}/>
	            <AccountList accounts={this.state.accounts}/>
			</div>
		)
	}
}
// end::app[]

// tag::atm-list[]
class AtmList extends React.Component{
	render() {
		var atms = this.props.atms.map(atm =>
			<Atm key={atm._links.self.href} atm={atm}/>
		);
		return (
			<table>
				<tbody>
					<tr>
						<th>Zip Code</th>
						<th>Address</th>
					</tr>
					{atms}
				</tbody>
			</table>
		)
	}
}
// end::atm-list[]

// tag::atm[]
class Atm extends React.Component{
	render() {
		return (
			<tr>
				<td>{this.props.atm.zipCode}</td>
				<td>{this.props.atm.address}</td>
			</tr>
		)
	}
}
// end::atm[]

// tag::account-list[]
class AccountList extends React.Component{
    render() {
        var accounts = this.props.accounts.map(account =>
            <Account key={account._links.self.href} account={account}/>
    );
        return (
            <table>
            <tbody>
            <tr>
            <th>Number</th>
            <th>Amount</th>
            </tr>
        {accounts}
    </tbody>
        </table>
    )
    }
}
// end::account-list[]

// tag::account[]
class Account extends React.Component{
    render() {
        return (
            <tr>
            <td>{this.props.account.number}</td>
        <td>{this.props.account.amount}</td>
        </tr>
    )
    }
}
// end::account[]
// tag::render[]
ReactDOM.render(
	<App />,
	document.getElementById('react')
)
// end::render[]

