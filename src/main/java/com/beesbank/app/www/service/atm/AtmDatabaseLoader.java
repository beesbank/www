package com.beesbank.app.www.service.atm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AtmDatabaseLoader implements CommandLineRunner {

    private final AtmRepository repository;

    @Autowired
    public AtmDatabaseLoader(AtmRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.repository.save(new Atm("95121", "2001 Gateway Place, San Jose"));
        this.repository.save(new Atm("95122", "1940 Airport Plaza, San Jose"));
        this.repository.save(new Atm("95123", "1234 Faceoogle Way, Mountain View"));
        this.repository.save(new Atm("95124", "4323 Goobook Way, Mountain View"));
    }
}