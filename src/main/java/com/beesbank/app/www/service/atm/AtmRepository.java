package com.beesbank.app.www.service.atm;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AtmRepository extends CrudRepository<Atm, Long> {
    List<Atm> findByZipCode(@Param("zipCode") String zipCode);
}
